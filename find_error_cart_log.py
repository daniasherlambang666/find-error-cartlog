def single(cart_code):
  import pandas as pd
  from dashboard.utils.date import (timedelta_to_hours, hours_to_timedelta)
  from datetime import datetime, timedelta
  from apps.models_helper.cart import (Cart, CartPhoto, CartTitipkirimin, CartDetail, CartLog, CartMovement, CartCost, CustomClearance)

  x = CartLog.objects.filter(cart__cart_code = cart_code)
  betul = []
  salah = []
  log = []
  hari = []
  cart_log_data_error = []
  cart_code_data_error = []
  for k in x: log.append( k.log_date)
  for k in x: hari.append(timedelta(days= hours_to_timedelta(k.lead_time)['days'] , hours= hours_to_timedelta(k.lead_time)['hours'], minutes= hours_to_timedelta(k.lead_time)['minutes']))

  for k in range(len(log)):
      try: 
              betul.append(log[k+1] + hari[k])
              salah.append(log[k])
      except:pass

  for b in range(len(betul)):
    try:
        if betul[b].strftime("%d") != salah[b].strftime("%d"):
              cart_error = CartLog.objects.filter(log_date = salah[b]).values_list('cart__cart_code', flat=True)
              log_error = CartLog.objects.filter(log_date = salah[b]).values_list('cart_log_id', flat=True)
              cart_log_data_error.append(log_error[0])
              
              if cart_error[0] not in cart_code_data_error:
                cart_code_data_error.append(cart_error[0])
    except: pass

  final_data_error = ''
  if len(cart_code_data_error) != 0 and len(cart_log_data_error) != 0:
    final_data_error = tuple(cart_code_data_error + cart_log_data_error)
    print(final_data_error)
    return final_data_error

def csv():
  import pandas as pd
  from apps.models_helper.cart import (Cart, CartPhoto, CartTitipkirimin, CartDetail, CartLog, CartMovement, CartCost, CustomClearance)
  
  x =Cart.objects.filter(status= 'finished').values_list('cart_code')

  input_csv = []
  for k in x :
      if single(k[0]) == None:
              pass
      else: input_csv.append(single(k[0]))
  pd.DataFrame(input_csv).to_csv("/home/x/data_cart_log_error.csv")
